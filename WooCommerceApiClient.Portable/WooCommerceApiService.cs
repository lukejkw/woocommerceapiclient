﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using WooCommerceApiClient.Models;

namespace WooCommerceApiClient
{
    public class WooCommerceApiService
    {
        #region Private Members

        private static object _Mutex = new object();
        private static WooCommerceApiService _Instance;

        #endregion

        #region Properties

        public string BaseUrl { get; private set; }
        public string CustomerKey { get; private set; }
        public string CustomerSecret { get; private set; }

        #endregion

        #region Constructors

        private WooCommerceApiService(string baseUrl, string custKey, string custSercret)
        {
            BaseUrl = baseUrl;
            CustomerKey = custKey;
            CustomerSecret = custSercret;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Instatiates the WooCommerceApiClient
        /// </summary>
        /// <param name="baseUrl">url of the website eg https://ticketstroom.com//wc-api/v3/</param>
        /// <param name="custKey"></param>
        /// <param name="custSercret"></param>
        /// <returns></returns>
        public static WooCommerceApiService Instance(string baseUrl, string custKey, string custSercret)
        {
            // Double Thread locked init func
            lock (_Mutex)
            {
                if (_Instance == null)
                {
                    lock (_Mutex)
                    {
                        _Instance = new WooCommerceApiService(baseUrl, custKey, custSercret);
                    }
                }
            }

            return _Instance;
        }

        /// <summary>
        /// Gets a list of orders from a woo commerce endpoint
        /// 
        /// Limited to 200 orders
        /// </summary>
        /// <returns>List of orders</returns>
        public async Task<List<Order>> GetOrdersAsync()
        {
            string json = await CallApiAsync(MethodType.GET, "orders?filter%5Blimit%5D=2000");
            OrderRoot orderData = JsonConvert.DeserializeObject<OrderRoot>(json);
            return orderData.orders;
        }

        /// <summary>
        /// Gets a list of products from a woo commerce endpoint
        /// 
        /// Limited to 2000 products
        /// </summary>
        /// <returns>List of products</returns>
        public async Task<List<Product>> GetProductsAsync()
        {
            string json = await CallApiAsync(MethodType.GET, "products?filter%5Blimit%5D=2000");
            ProductRoot productData = JsonConvert.DeserializeObject<ProductRoot>(json);
            return productData.products;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Calls the woocommerce service using basic authentication
        /// </summary>
        /// <param name="method">Enum value indicating the request method</param>
        /// <param name="partialUrl">Indicates the woo commerce data type. eg orders, products</param>
        /// <param name="jsonData">Data used for POST requests - OPTIONAL</param>
        /// <returns>returned json string</returns>
        private async Task<string> CallApiAsync(MethodType method, string partialUrl, string jsonData = "")
        {
            // Build Url
            string url = $"{BaseUrl}/{partialUrl}";

            // Create HttpClient with Handler
            HttpClientHandler handler = new HttpClientHandler()
            {
                // Set basic auth creds
                Credentials = new NetworkCredential(CustomerKey, CustomerSecret)
            };
            using (var client = new HttpClient(handler))
            {
                // Set encoded request header values
                var byteArray = new UTF8Encoding().GetBytes($"{CustomerKey}:{CustomerSecret}");
                string authValue = Convert.ToBase64String(byteArray);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authValue);

                // Take action
                switch (method)
                {
                    case MethodType.GET:
                        // Get response and content
                        HttpResponseMessage getResponse = await client.GetAsync(url);
                        HttpContent getContent = getResponse.Content;
                        return await getContent.ReadAsStringAsync();
                    case MethodType.POST:
                        // TODO
                        throw new NotImplementedException();
                    case MethodType.HEAD:
                        throw new NotImplementedException();
                    case MethodType.PUT:
                        throw new NotImplementedException();
                    default:
                        throw new ArgumentException();
                }
            }
        }

        #endregion
    }
}