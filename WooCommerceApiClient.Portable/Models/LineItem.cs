﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WooCommerceApiClient.Models
{
    public class LineItem
    {
        public int id { get; set; }
        public string subtotal { get; set; }
        public string subtotal_tax { get; set; }
        public string total { get; set; }
        public string total_tax { get; set; }
        public string price { get; set; }
        public int quantity { get; set; }
        public object tax_class { get; set; }
        public string name { get; set; }
        public int product_id { get; set; }
        public string sku { get; set; }
        public List<object> meta { get; set; }
    }
}
