﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WooCommerceApiClient.Models
{
    public class ProductRoot
    {
        public List<Product> products { get; set; }
    }
}
