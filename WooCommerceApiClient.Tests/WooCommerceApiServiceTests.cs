﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using WooCommerceApiClient.Models;

namespace WooCommerceApiClient.Tests
{
    [TestClass]
    public class WooCommerceApiServiceTest
    {
        // Enter creds
        const string StoreUrl = "";
        const string CustKey = "";
        const string CustSecret = "";

        WooCommerceApiService _Service = WooCommerceApiService.Instance($"{ StoreUrl}/wc-api/v3", CustKey, CustSecret);

        [TestMethod]
        public void TestCanGetOrders()
        {
            List<Order> orders = _Service.GetOrdersAsync().Result;

            Assert.IsNotNull(orders);
        }

        [TestMethod]
        public void TestCanGetProducts()
        {
            List<Product> products = _Service.GetProductsAsync().Result;

            Assert.IsNotNull(products);
        }
    }
}
